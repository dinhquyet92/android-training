package com.example.emptyappjava.interfaces;

public interface NewsOnClick {
    void onClickItem(int position);
}
