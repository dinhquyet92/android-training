package com.example.emptyappjava.activity.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.emptyappjava.R;

public class HomeFragment extends Fragment {
    TextView tv;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        tv = root.findViewById(R.id.text_home);
        tv.setText("HOME FRAGMENT");

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        tv.setText("HOME FRAGMENT onResume");
    }

    @Override
    public void onStart() {
        super.onStart();
        tv.setText("HOME FRAGMENT onStart");
    }
}