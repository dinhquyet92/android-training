package com.example.emptyappjava.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.example.emptyappjava.R;
import com.example.emptyappjava.adapter.ProductAdapter;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ProductActivity extends AppCompatActivity {

    private List<Product> listProduct = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        // B1: Data source
        initData();

        // B2: Adapter
        ProductAdapter adapter = new ProductAdapter(this, listProduct);

        // B3: Layout Manager
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        // B4: RecycleView
        XRecyclerView rvProduct = findViewById(R.id.rvProduct);
        rvProduct.setLayoutManager(layoutManager);
        rvProduct.setAdapter(adapter);

        // lazy load
        rvProduct.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                Log.d("XRecyclerView", "onRefresh");
            }

            @Override
            public void onLoadMore() {
                Log.d("XRecyclerView", "onLoadMore");
            }
        });

    }

    private void initData(){
        for (int i = 1; i<=6; i++){
            listProduct.add(new Product("Product 0"+i, 999999990+i, R.drawable.p1+i));
        }

    }
}