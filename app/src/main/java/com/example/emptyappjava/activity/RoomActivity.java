package com.example.emptyappjava.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.net.CaptivePortal;
import android.os.Bundle;
import android.util.Log;

import com.example.emptyappjava.R;
import com.example.emptyappjava.database.AppDatabase;
import com.example.emptyappjava.database.BookmarkEntity;

import java.util.List;

public class RoomActivity extends AppCompatActivity {

    AppDatabase appDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        appDatabase = AppDatabase.getAppDatabase(this);
        insertBookmark();
        updateBookmark(1);
        getAllBookmark();
    }

    private void insertBookmark(){
        BookmarkEntity bm = new BookmarkEntity();
        bm.title = "Title 1";
        bm.content = "Content 1";
        appDatabase.bookmarkDao().insertBookmark(bm);
    }
    private void updateBookmark(int id){
        BookmarkEntity bm = appDatabase.bookmarkDao().getBookmark(id);
        bm.title = "updated title 1";
        appDatabase.bookmarkDao().updateBookmark(bm);
    }
    private void getAllBookmark(){
        List<BookmarkEntity> list   = appDatabase.bookmarkDao().getAllBookmark();
        for (BookmarkEntity model : list){
            Log.d("Bookmark", "id: "+model.id+", title: "+model.title+"");
        }
    }
}