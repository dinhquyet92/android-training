package com.example.emptyappjava.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emptyappjava.R;
import com.example.emptyappjava.adapter.NewsAdapter;
import com.example.emptyappjava.interfaces.NewsOnClick;
import com.example.emptyappjava.model.Item;
import com.example.emptyappjava.network.APIManager;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkRecycleView extends AppCompatActivity {

    TextView tvDate, tvTitle, tvContent;
    ImageView ivImage;
    List<Item> listNews;
    NewsAdapter newsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_recycle_view);
        getSupportActionBar().setTitle("DanTri.vn");

        tvDate = findViewById(R.id.tvDate);
        tvTitle = findViewById(R.id.tvTitle);
        tvContent = findViewById(R.id.tvContent);
        ivImage = findViewById(R.id.ivImage);
        // B1: Data source
        this.getData();
        // B2: Adapter
        listNews = new ArrayList<>();
        newsAdapter = new NewsAdapter(NetworkRecycleView.this, listNews);

        // B3: Layout manager
        XRecyclerView.LayoutManager layoutManager = new LinearLayoutManager(NetworkRecycleView.this);

        // B4: RecycleView
        XRecyclerView xRecyclerView = findViewById(R.id.rvNews);
        xRecyclerView.setLayoutManager(layoutManager);
        xRecyclerView.setAdapter(newsAdapter);
//Bind onclick event
        newsAdapter.setiOnClick(new NewsOnClick() {
            @Override
            public void onClickItem(int position) {
                Item model = listNews.get(position);
                Intent intent = new Intent(NetworkRecycleView.this, DetailActivity.class);
                //intent.putExtra("url", model.getContent().getUrl());
                intent.putExtra("item", model);
                startActivity(intent);
            }
        });
        // Lazy load
        xRecyclerView.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                Log.d("LoadingListener", "onRefresh");
            }

            @Override
            public void onLoadMore() {
                Log.d("LoadingListener", "onLoadMore");
            }
        });
    }

    private void getData(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIManager.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIManager service = retrofit.create(APIManager.class);
        service.getListData().enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                if (response.body() == null){
                    return;
                }
                listNews = response.body();
                newsAdapter.reloadData(listNews);
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                Toast.makeText(NetworkRecycleView.this, "getListData FAILED: ", Toast.LENGTH_SHORT);
            }
        });
    }
}