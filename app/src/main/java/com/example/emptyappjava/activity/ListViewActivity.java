package com.example.emptyappjava.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.emptyappjava.R;

public class ListViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        // B1: data
        final String[] data = {"SAMSUNG", "SONY", "APPLE"};

        // B2: Adapter
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, data);

        // ListView
        ListView lvSimple = findViewById(R.id.lvSimple);
        lvSimple.setAdapter(adapter);

        lvSimple.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                myToast.cancel();
                String item = data[i];
//                myToast = Toast.makeText(ListViewActivity.this, item, Toast.LENGTH_SHORT);
                Toast.makeText(ListViewActivity.this, item, Toast.LENGTH_SHORT).show();
//                myToast.show();
            }
        });
    }
}