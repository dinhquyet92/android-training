package com.example.emptyappjava.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.example.emptyappjava.R;
import com.example.emptyappjava.database.AppDatabase;
import com.example.emptyappjava.database.BookmarkEntity;
import com.example.emptyappjava.model.Item;
import com.example.emptyappjava.network.APIManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class DetailActivity extends AppCompatActivity {
    FloatingActionButton btnBookmark;
    Boolean isBookmarked = false;
    AppDatabase appDatabase;
    Item currentNews;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getSupportActionBar().setTitle("Detail News");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // show web view
        Intent intent = getIntent();
        currentNews = (Item) intent.getSerializableExtra("item");
        //String url = intent.getStringExtra("url");
        String link = APIManager.BASE_URL + currentNews.getContent().getUrl().toString();
        WebView webView = findViewById(R.id.wvDetail);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(link);

        // connect Database
        appDatabase = AppDatabase.getAppDatabase(this);

        // bookmark
        btnBookmark = findViewById(R.id.btnBookmark);
        // check bookmark
        isBookmarked = checkBookmark(currentNews.getId());
        if (isBookmarked){
            btnBookmark.setImageResource(R.drawable.bookmarked);
        }else {
            btnBookmark.setImageResource(R.drawable.bookmark);
        }
        // bind click bookmark
        btnBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isBookmarked){
                    deleteBookmark(currentNews.getId());
                    btnBookmark.setImageResource(R.drawable.bookmark);
                }else {
                    insertBookmark(currentNews);
                    btnBookmark.setImageResource(R.drawable.bookmarked);
                }
                isBookmarked = !isBookmarked;
            }
        });

    }

    private void insertBookmark(Item item){
        BookmarkEntity bm = new BookmarkEntity(item);
//        bm.title = item.getTitle();
//        bm.content = item.getContent().getDescription();
//        bm.url = item.getContent().getUrl();
//        bm.date = item.getDate();
//        bm.id = item.getId();
//        bm.image = item.getImage();
        appDatabase.bookmarkDao().insertBookmark(bm);
        Toast.makeText(this, "Bookmarked this url successfully!", Toast.LENGTH_SHORT);
    }

    private void deleteBookmark(int id){
        BookmarkEntity bm = new BookmarkEntity();
        bm = appDatabase.bookmarkDao().getBookmark(id);
        appDatabase.bookmarkDao().deleteBookmark(bm);
        Toast.makeText(this, "Removed this url from your bookmark successfully!", Toast.LENGTH_SHORT);
    }

    private boolean checkBookmark(int id){
        BookmarkEntity bm = appDatabase.bookmarkDao().getBookmark(id);
        return (bm != null);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //return super.onOptionsItemSelected(item);
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}