package com.example.emptyappjava.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.emptyappjava.R;

public class MainActivity extends AppCompatActivity {

    public View ivStar;
    public View btLogin;
    public Button btnLike;
    private TextView tvNick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook);
        btnLike = findViewById(R.id.btnLike);
        btnLike.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                btnLike.setText("LIKED");
            }
        });
//        get data from Login Activity
        Intent data = getIntent();
        String user = data.getStringExtra("user");
        tvNick = findViewById(R.id.tvNick);
        tvNick.setText(user);
        Log.d("MainActivity", "onCreate");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("MainActivity", "onRestart");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MainActivity", "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MainActivity", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MainActivity", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("MainActivity", "onDestroy");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d("MainActivity", "onBackPressed");
    }
}