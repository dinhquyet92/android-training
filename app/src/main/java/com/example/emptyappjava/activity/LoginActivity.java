package com.example.emptyappjava.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.emptyappjava.R;

public class LoginActivity extends AppCompatActivity {
    private EditText edUser;
    private EditText edPass;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLogin = findViewById(R.id.btnLogin);
        edUser = findViewById(R.id.edUsername);
        edPass = findViewById(R.id.edPassword);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveStateLogin();
                goMain();
            }
        });

        if(checkLogin()){
            goMain();
        }
        Log.d("LoginActivity", "onCreate");
    }
    private boolean checkLogin(){
        SharedPreferences setting = getSharedPreferences("Login", MODE_PRIVATE);
        boolean isLogin = setting.getBoolean("IS_LOGIN", false);
        return isLogin;
    }
    private void saveStateLogin(){
        SharedPreferences settings = getSharedPreferences("Login", MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("IS_LOGIN", true);
        editor.apply();
    }
    private void goMain(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("user", edUser.getText().toString());
        startActivity(intent);
        finish();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("LoginActivity", "onRestart");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("LoginActivity", "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("LoginActivity", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("LoginActivity", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("LoginActivity", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("LoginActivity", "onDestroy");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d("LoginActivity", "onBackPressed");
    }
}