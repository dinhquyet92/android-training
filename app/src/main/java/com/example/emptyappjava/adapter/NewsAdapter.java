package com.example.emptyappjava.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.emptyappjava.R;
import com.example.emptyappjava.interfaces.NewsOnClick;
import com.example.emptyappjava.model.Item;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.List;

public class NewsAdapter extends XRecyclerView.Adapter {

    private Activity activity;
    private List<Item> listNews;
    private NewsOnClick  iOnClick;

    public NewsAdapter(Activity activity, List<Item> listNews) {
        this.activity = activity;
        this.listNews = listNews;
    }

    public void setiOnClick(NewsOnClick iOnClick) {
        this.iOnClick = iOnClick;
    }

    public void reloadData(List<Item> listItem){
        this.listNews = listItem;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = activity.getLayoutInflater().inflate(R.layout.news_item, parent, false);
        NewsHolder holder = new NewsHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        NewsHolder hd = (NewsHolder) holder;
        Item model = listNews.get(position);
        hd.tvTitle.setText(model.getTitle());
        hd.tvDate.setText(model.getDate());
        hd.tvContent.setText(model.getContent().getDescription());
        Glide.with(activity).load(model.getImage()).into(hd.ivImage);
    }

    @Override
    public int getItemCount() {
        return listNews.size();
    }

    private class NewsHolder extends RecyclerView.ViewHolder{
        TextView tvTitle, tvDate, tvContent;
        ImageView ivImage;
        public NewsHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvContent = itemView.findViewById(R.id.tvContent);
            ivImage = itemView.findViewById(R.id.ivImage);

            // B1
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("setOnClickListener", getAdapterPosition()+"");
                    iOnClick.onClickItem(getAdapterPosition()-1);
                }
            });
        }
    }
}
