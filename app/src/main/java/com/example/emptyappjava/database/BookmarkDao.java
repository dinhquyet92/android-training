package com.example.emptyappjava.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface BookmarkDao {
    @Insert(onConflict = REPLACE)
    void insertBookmark(BookmarkEntity bookmarkEntity);

    @Update
    void updateBookmark(BookmarkEntity bookmarkEntity);

    @Delete
    void deleteBookmark(BookmarkEntity bookmarkEntity);

    @Query("Select * from bookmark")
    List<BookmarkEntity> getAllBookmark();

    @Query("Select * from bookmark where id= :id")
    BookmarkEntity getBookmark(int id);

    @Query("DELETE FROM bookmark")
    void deleteAllBookmark();
}
