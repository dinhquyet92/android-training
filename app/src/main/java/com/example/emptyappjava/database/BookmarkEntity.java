package com.example.emptyappjava.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.emptyappjava.model.Item;

import java.io.Serializable;

@Entity(tableName = "bookmark")
public class BookmarkEntity {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "title")
    public String title;


    @ColumnInfo(name = "date")
    public String date;


    @ColumnInfo(name = "content")
    public String content;


    @ColumnInfo(name = "url")
    public String url;


    @ColumnInfo(name = "image")
    public String image;

    public BookmarkEntity() {
    }

    public BookmarkEntity(int id, String title, String date, String content, String url, String image) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.content = content;
        this.url = url;
        this.image = image;
    }

    public BookmarkEntity(Item item) {
        this.id = item.getId();
        this.title = item.getTitle();
        this.date = item.getDate();
        this.content = item.getContent().getDescription();
        this.url = item.getContent().getUrl();
        this.image = item.getImage();
    }
}
